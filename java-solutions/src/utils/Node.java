package utils;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import static java.util.function.Predicate.not;

/**
 * This is shitty implementation
 * @param <N>
 * @param <T>
 */
@Deprecated
public abstract class Node<N extends Node, T> {
    private boolean visited = false;
    private final List<N> children = new ArrayList<>();
    private T data;

    public Node(T data) {
        this.data = data;
    }

    public List<N> getChildren() {
        return children;
    }

    public List<N> getUnvisitedChildren() {
        return children.stream()
                .filter(not(Node::isVisited))
                .collect(Collectors.toList());
    }


    public Optional<N> getFirstUnvisitedChild() {
        return getUnvisitedChildren().stream()
                .findFirst();
    }

    public void setVisited(boolean v) {
        visited = v;
    }

    public boolean isVisited() {
        return visited;
    }

    public void setData(T d) {
        data = d;
    }

    public T getData() {
        return data;
    }

    @Override
    public String toString() {
        return data.toString();
    }
}
