package utils

fun <T> exec(expected: Int, executor: () -> T) {

    val result = executor()

    if (result == expected) {
        println("success! Got: $result")
    } else {
        println("failure! Expected $expected and got: $result !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!")
    }
    println("--------------------------------------------------------------------------")
}