package utils;

import java.util.Objects;

public class Position implements Comparable<Position> {
    public int x;
    public int y;

    public Position(int x, int y) {
        this.x = x;
        this.y = y;
    }

    @Override
    public boolean equals(Object obj) {
        if(!(obj instanceof Position)) {
            return false;
        }

        var pos = (Position) obj;

        return x == pos.x && y == pos.y;
    }

    @Override
    public int hashCode() {
        return Objects.hash(x, y);
    }

    @Override
    public int compareTo(Position o) {

        if (equals(o)) {
            return 0;
        }

        return Math.max(o.x - this.x, o.y - this.y);
    }

    @Override
    public String toString() {
        return String.format("x: %d, y: %d", x, y);
    }

    public Position withXDiff(int diff) {
        return new Position(x + diff, y);
    }

    public Position withYDiff(int diff) {
        return new Position(x, y + diff);
    }

}
