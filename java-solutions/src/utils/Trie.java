package utils;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.stream.Collectors;

public class Trie {
    Node root;

    public Trie() {
        root = new Trie.Node();
    }

    public static class Node {
        HashMap<Character, Node> children = new HashMap<>();
        String content;
        boolean isWord = false;
    }

    public void add(String word) {
        var node = root;
        var i = 0;
        while (i < word.length()) {
            var c = word.charAt(i);
            node = node.children.computeIfAbsent(c, key -> new Trie.Node());
            i++;
        }

        node.content = word;
        node.isWord = true;
    }

    public boolean contains(String word) {
        var node = root;
        var i = 0;
        while (node != null && i < word.length()) {
            var c = word.charAt(i);
            node = node.children.get(c);
            i++;
        }

        return node != null;
    }

    public List<String> search(String word) {
        var node = root;
        var i = 0;
        while (node != null && i < word.length()) {
            var c = word.charAt(i);
            node = node.children.get(c);
            i++;
        }

        if (node == null) {
            return new LinkedList<>();
        }

        return listOfWords(node);
    }

    public static List<String> listOfWords(Node node) {
        List<String> words = new ArrayList<>();

        if (node.isWord) {
            words.add(node.content);
        }

        words.addAll(node.children.values().stream()
                .flatMap(n -> listOfWords(n).stream())
                .collect(Collectors.toList()));

        return words;
    }
}
