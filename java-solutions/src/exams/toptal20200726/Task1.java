package exams.toptal20200726;

import java.util.HashMap;
import java.util.Objects;
import java.util.concurrent.TimeUnit;
import java.util.function.Supplier;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

public class Task1 {

    static class Solution {
        /**
         * @param S list of chars representing points tag names
         * @param X list of X coordinates
         * @param Y list of Y coordinates
         * @return number of points inside circle
         */
        public int solution(String S, int[] X, int[] Y) {
            return time("calculating solution", () -> {
                HashMap<Character, Point> visitedPoints = new HashMap<>();
                int pointsInsideCircle = 0;
                double maxR = Double.MAX_VALUE;

                var points = IntStream.range(0, X.length)
                        .mapToObj(i -> new Point(X[i], Y[i], S.charAt(i)))
                        .collect(Collectors.toList());

                for (Point p : points) {
                    var visitedPoint = visitedPoints.get(p.tag);
                    if (visitedPoint != null) {
                        maxR = Math.min(p.radius(), maxR);
                        if (visitedPoint.equals(p)) { //same radius
                            print("removing " + p.toString());
                            visitedPoints.remove(p.tag);
                            pointsInsideCircle--;
                        } else if (visitedPoint.radius() > p.radius()) {
                            print("replacing " + p.toString());
                            visitedPoints.replace(p.tag, p);
                        } else {
                            print("skipping " + p.toString());
                        }

                        continue;
                    }

                    print("adding " + p.toString());
                    pointsInsideCircle++;
                    visitedPoints.put(p.tag, p);
                }

                print("max r " + maxR);

                for (Point p : visitedPoints.values()) {
                    if (p.radius() > maxR) {
                        print("removing (second loop) " + p);
                        pointsInsideCircle--;
                    }
                }

                return pointsInsideCircle;
            });
        }

        public static class Point implements Comparable<Point>{
            public static final int COMPARE_TO_PRECISION = 1000;
            int x;
            int y;
            double r;
            char tag;

            public static Point of(char tag, int x, int y) {
                return new Point(x, y, tag);
            }

            public Point(int x, int y, char tag) {
                this.x = x;
                this.y = y;
                this.r = Math.sqrt(Math.pow(x, 2) + Math.pow(y, 2));
                this.tag = tag;
            }

            @Override
            public int compareTo(Point o) {
                return (int) Math.floor(radius() * COMPARE_TO_PRECISION - o.radius() * COMPARE_TO_PRECISION);
            }

            @Override
            public boolean equals(Object obj) {
                var other = (Point) obj;
                return Objects.equals(tag, other.tag) && Objects.equals(radius(), other.radius());
            }

            @Override
            public int hashCode() {
                return Objects.hash(x, y, tag);
            }

            @Override
            public String toString() {
                return String.format("%s [%d,%d] (%f)", tag, x, y, r);
            }

            public double radius() {
                return r;
            }

            public boolean isInsideCircle(int r) {
                return radius() <= r;
            }
        }
    }

    public static void main(String[] args) {
        solution(3,
                Solution.Point.of('A', 2, 2),
                Solution.Point.of('B', -1, -2),
                Solution.Point.of('D', -4, 4),
                Solution.Point.of('C', -3, 1),
                Solution.Point.of('A', 3, 3)
        );
        solution(1,
                Solution.Point.of('A', 1, 1),
                Solution.Point.of('B', -2, -2),
                Solution.Point.of('B', -2, 2)
        );
        solution(0,
                Solution.Point.of('A', 1, 1),
                Solution.Point.of('B', -2, -2),
                Solution.Point.of('B', -2, 2),
                Solution.Point.of('A', -1, 1)
        );
        solution(6,
                Solution.Point.of('A', 1, 1),
                Solution.Point.of('B', 1, 1),
                Solution.Point.of('C', 1, 1),
                Solution.Point.of('D', 1, 1),
                Solution.Point.of('E', 1, 1),
                Solution.Point.of('F', 1, 1)
        );

        solution(-1, random(1_000_000));
    }

    public static void solution(int expected, Solution.Point... points) {
        String tags = "";
        int size = points.length;
        int[] xs = new int[size];
        int[] ys = new int[size];
        for (int i = 0; i < points.length; i++) {
            Solution.Point p = points[i];
            tags += p.tag;
            xs[i] = p.x;
            ys[i] = p.y;
        }
        var result = new Solution().solution(tags, xs, ys);
        printInfo(result);
        assert result == expected;
    }

    public static Solution.Point[] random(int count) {
        return time("generate random points", () -> IntStream.range(0, count - 1)
                .mapToObj(i -> {
                    var tag = (char) (Math.floor(Math.random() * ('Z' - 'A')) + 'A');
                    var x = (int) (Math.signum(Math.random() - 0.5) * Math.random() * 100_000_000);
                    var y = (int) (Math.signum(Math.random() - 0.5) * Math.random() * 100_000_000);
                    return Solution.Point.of(tag, x, y);
                })).toArray(Solution.Point[]::new);
    }

    public static <T> T time(String title, Supplier<T> supp) {
        var start = System.nanoTime();
        try {
            return supp.get();
        } finally {
            printInfo(String.format("%s took %d", title, TimeUnit.NANOSECONDS.toMillis(System.nanoTime() - start)));
        }
    }

    public static void print(Object o) {
        //System.out.println(o);
    }

    public static void printInfo(Object o) {
        System.out.println(o);
    }
}
