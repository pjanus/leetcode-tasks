package tasks.chocolatesByNumber

fun solution(N: Int, M: Int): Int {
    println("N: $N, M: $M")
    return N / gcd(N, M)
}

fun gcd(a: Int, b: Int): Int {
    if (a % b == 0) {
        return b
    }

    return gcd(b, a % b)
}