package tasks.chocolatesByNumber

import utils.exec

fun main() {
    exec(5) {  solution(10, 4) }
    exec(1_000_000) {  solution(1_000_000_000, 333_000) }
    exec(1) {  solution(1_000_000_000, 1_000_000_000) }
    exec(1) {  solution(415633212, 234332) }
}