package tasks;

import utils.Position;

import java.util.*;

public class NumberOfIslands {

    public static void main(String[] args) {
        char[][] grid = new char[][] {
                new char[] {'1','1','1','1','0'},
                new char[] {'1','1','0','1','0'},
                new char[] {'1','1','0','0','0'},
                new char[] {'0','0','0','0','0'},
        };

        test(numIslands(grid), 1);

        char[][] grid2 = new char[][] {
                new char[] {'1','1','0','0','0'},
                new char[] {'1','1','0','0','0'},
                new char[] {'0','0','1','0','0'},
                new char[] {'0','0','0','1','1'},
        };

        test(numIslands(grid2), 3);

        char[][] grid3 = new char[][] {
                new char[]{'1','1','1'},
                new char[]{'0','1','0'},
                new char[]{'1','1','1'}
        };

        test(numIslands(grid3), 1);
        
        char[][] grid4 = new char[][] {
                new char[]{'1','1','1','1','1','0','1','1','1','1','1','1','1','1','1','0','1','0','1','1'},
                new char[]{'0','1','1','1','1','1','1','1','1','1','1','1','1','0','1','1','1','1','1','0'},
                new char[]{'1','0','1','1','1','0','0','1','1','0','1','1','1','1','1','1','1','1','1','1'},
                new char[]{'1','1','1','1','0','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1'},
                new char[]{'1','0','0','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1'},
                new char[]{'1','0','1','1','1','1','1','1','0','1','1','1','0','1','1','1','0','1','1','1'},
                new char[]{'0','1','1','1','1','1','1','1','1','1','1','1','0','1','1','0','1','1','1','1'},
                new char[]{'1','1','1','1','1','1','1','1','1','1','1','1','0','1','1','1','1','0','1','1'},
                new char[]{'1','1','1','1','1','1','1','1','1','1','0','1','1','1','1','1','1','1','1','1'},
                new char[]{'1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1'},
                new char[]{'0','1','1','1','1','1','1','1','0','1','1','1','1','1','1','1','1','1','1','1'},
                new char[]{'1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1'},
                new char[]{'1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1'},
                new char[]{'1','1','1','1','1','0','1','1','1','1','1','1','1','0','1','1','1','1','1','1'},
                new char[]{'1','0','1','1','1','1','1','0','1','1','1','0','1','1','1','1','0','1','1','1'},
                new char[]{'1','1','1','1','1','1','1','1','1','1','1','1','0','1','1','1','1','1','1','0'},
                new char[]{'1','1','1','1','1','1','1','1','1','1','1','1','1','0','1','1','1','1','0','0'},
                new char[]{'1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1'},
                new char[]{'1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1'},
                new char[]{'1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1'}
        };

        test(numIslands(grid4), 1);
    }

    public static void test(int actual, int expected) {
        String result = "";
        result = actual == expected
                ? actual + " Git! :)"
                : actual + " Lipa! :( Powinno być " + expected;

        System.out.println(result);
    }

    public static int numIslands(char[][] gridArr) {
        int count = 0;
        final int limitY = gridArr.length;
        final int limitX = limitY > 0 ? gridArr[0].length : 0;

        if (limitX == 0) {
            return 0;
        }

        final var queue = new LinkedList<Position>();

        for (int y = 0; y < limitY; y++) {
            for (int x = 0; x < limitX; x++) {
                var node = gridArr[y][x];

                if (node == '1') {
                    count++;
                    queue.offer(new Position(x, y));

                    while (!queue.isEmpty()) {
                        final var pos = queue.poll();
                        if (gridArr[pos.y][pos.x] == 'v') {
                            continue;
                        }
                        gridArr[pos.y][pos.x] = 'v';
                        final var top = pos.withYDiff(-1);
                        final var left = pos.withXDiff(-1);
                        final var bottom = pos.withYDiff(1);
                        final var right = pos.withXDiff(1);

                        if(isPartOfCurrentIsland(top, limitX, limitY, gridArr)) {
                            queue.offer(top);
                        }

                        if(isPartOfCurrentIsland(left, limitX, limitY, gridArr)) {
                            queue.offer(left);
                        }

                        if(isPartOfCurrentIsland(bottom, limitX, limitY, gridArr)) {
                            queue.offer(bottom);
                        }

                        if(isPartOfCurrentIsland(right, limitX, limitY, gridArr)) {
                            queue.offer(right);
                        }
                    }
                }
            }
        }
        return count;
    }

    private static boolean isPartOfCurrentIsland(Position pos, int limitX, int limitY, char[][] gridArr) {
        return pos.x >= 0 && pos.x < limitX &&
                pos.y >= 0 && pos.y < limitY &&
                gridArr[pos.y][pos.x] == '1';
    }
}
