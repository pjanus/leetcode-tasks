package tasks;

import java.util.Objects;
import java.util.stream.Stream;

public class LongestPassword {

    public static void main(String[] args) {
        System.out.println(new Solution().solution("Test1 wow dupa 1123312222221"));
        System.out.println(new Solution().solution("1"));
        System.out.println(new Solution().solution("a"));
    }

    static class Solution {
        public int solution(String S) {
            return Stream.of(S.split(" "))
                    .parallel()
                    .map(String::trim)
                    .map(Strength::new)
                    .sorted()
                    .filter(Strength::isValid)
                    .findFirst()
                    .map(Strength::getPower)
                    .orElse(-1);
        }

        static class Strength implements Comparable<Strength>{
            private final String s;
            private int letters = 0;
            private int digits = 0;
            private boolean valid = false;

            public Strength(String s) {
                this.s = s;
            }

            public boolean isValid() {
                test();
                return valid;
            }

            public int getPower() {
                return s.length();
            }

            @Override
            public int compareTo(Strength second) {
                //strongestFirst

                if (this.getPower() > second.getPower()) {
                    return -1;
                }

                if (this.getPower() < second.getPower()) {
                    return 1;
                }

                return 0;
            }

            @Override
            public boolean equals(Object obj) {
                return s.equals(obj);
            }

            @Override
            public int hashCode() {
                return s.hashCode();
            }

            private void test() {
                for (char c : s.toCharArray()) {

                    if (isLetter(c)) {
                        letters++;
                        continue;
                    }

                    if (isDigit(c)) {
                        digits++;
                        continue;
                    }

                    return;
                }

                if (letters % 2 == 1) {
                    return;
                }

                if (digits % 2 == 0) {
                    return;
                }

                valid = true;
            }

            private boolean isLetter(char c) {
                return (c >= 'a' && c <= 'z') ||
                        (c >= 'A' && c <= 'Z');
            }

            private boolean isDigit(char c) {
                return (c >= '0' && c <= '9');
            }
        }
    }
}
