package tasks;

import java.util.Arrays;

public class MissingInteger {

    public static void main(String[] args) {
        int[] test1 = {1, 2, 3};
        int[] test2 = {-1000, 1000};
        int[] test3 = {-1, -2, -1, 2, 3, 4, 5, 6, -5, -212132, 7, 1};
        System.out.println(solution(test1));
        System.out.println(solution(test2));
        System.out.println(solution(test3));
    }

    public static int solution(int[] A) {
        boolean[] visited = new boolean[1_000_001];
        Arrays.fill(visited, false);

        for (int i = 0; i < A.length; i++) {
            int it = A[i];
            if (it > 0) {
                visited[it] = true;
            }
        }

        for (int i = 1; i < visited.length; i++) {
            boolean it = visited[i];
            if (!it) {
                return i;
            }
        }

        return visited.length;
    }
}
