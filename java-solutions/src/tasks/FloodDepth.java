package tasks;

// you can also use imports, for example:
import java.util.*;

// you can write to stdout for debugging purposes, e.g.
// System.out.println("this is a debug message");

public class FloodDepth {

    public static void main(String[] args) {
        System.out.println(new Solution().solution(new int[] {3, 0, 3}));
    }

    static class Solution {
        public int solution(int[] A) {
            var depth = 0;
            var result = 0;
            var startIdx = 0;
            var endIdx = 1;

            if (A.length < 3) {
                return result;
            }

            while(startIdx < endIdx && startIdx < A.length) {
                var start = A[startIdx];
                var bottom = start;
                while (endIdx < A.length) {
                    var end = A[endIdx];
                    print("in: {start: %d [%d], end: %d [%d]}",
                            startIdx, start, endIdx, end);

                    if (start > end) {
                        endIdx++;
                        bottom = Math.min(bottom, end);
                    }

                    if (end > bottom) {
                        depth = Math.max(Math.min(end, start) - bottom, depth);
                        result = Math.max(result, depth);
                    }

                    print("out: {bottom: %d, depth: %d, maxDepth: %d}", bottom, depth, result);

                    if (start <= end) {
                        break;
                    }
                }
                startIdx=endIdx;
                endIdx++;
            }
            return result;
        }

        public void print(String str, Object... args) {
            System.out.println(String.format(str, args));
        }
    }
}
