package tasks;

import utils.Node;

import java.util.*;
import java.util.function.BiConsumer;
import java.util.stream.Collectors;

public class CriticalRouters {

    // Input: numNodes = 7, numEdges = 7, edges = [[0, 1], [0, 2], [1, 3], [2, 3], [2, 5], [5, 6], [3, 4]]

    public static void main(String[] args) {
        int numNodes = 7;
        int numEdges = 7;
        var edges = List.of(
                List.of(0, 1),
                List.of(0, 2),
                List.of(1, 3),
                List.of(2, 3),
                List.of(2, 5),
                List.of(5, 6),
                List.of(3, 4)
        );


        System.out.println(solve(numNodes, numEdges, edges));
    }

    private static String solve(int numNodes, int numEdges, List<List<Integer>> edges) {
        var criticalNodes = new LinkedList<Integer>();

        System.out.println("originalArray " + isConnected(numNodes, edges));

        for (var i = 0; i < numNodes; i++) {
            List<List<Integer>> copy = withoutNode(edges, i);

            if(!isConnected(numNodes - 1, copy)) {
                criticalNodes.add(i);
            }
        }

        return criticalNodes.stream()
                .map(Object::toString)
                .collect(Collectors.joining(", "));
    }

    private static List<List<Integer>> withoutNode(List<List<Integer>> edges, int i) {
        return edges.stream()
                .filter(e -> e.get(0) != i && e.get(1) != i)
                .collect(Collectors.toList());
    }

    public static boolean isConnected(int numNodes, List<List<Integer>> edges) {
        var dfs = dfs(edges);
        var bfs = bfs(edges);
        return dfs.size() == numNodes && bfs.size() == numNodes;
    }

    public static Queue<RouterNode> bfs(List<List<Integer>> edges) {
        HashMap<Integer, RouterNode> nodeMap = buildNodeMap(edges);
        var root = nodeMap.values().stream().findFirst().get();
        return graph(root, List::addAll);
    }

    public static Queue<RouterNode> dfs(List<List<Integer>> edges) {
        HashMap<Integer, RouterNode> nodeMap = buildNodeMap(edges);
        var root = nodeMap.values().stream().findFirst().get();

        return graph(root, (list, nodes) -> {
            list.addAll(0, nodes);
        });
    }

    private static HashMap<Integer, RouterNode> buildNodeMap(List<List<Integer>> edges) {
        var nodeMap = new HashMap<Integer, RouterNode>();
        edges.forEach(e -> {
            var idx = e.get(0);
            var idx2 = e.get(1);
            var node1 = nodeMap.getOrDefault(idx, new RouterNode(idx));
            var node2 = nodeMap.getOrDefault(idx2, new RouterNode(idx2));
            nodeMap.put(idx, node1);
            nodeMap.put(idx2, node2);

            node1.getChildren().add(node2);
            node2.getChildren().add(node1);
        });
        return nodeMap;
    }

    public static Queue<RouterNode> graph(RouterNode root, BiConsumer<List<RouterNode>, List<RouterNode>> fn) {
        var queue = new LinkedList<RouterNode>();
        var visitedNodes = new LinkedList<RouterNode>();
        queue.add(root);

        while (!queue.isEmpty()) {
            var node = queue.poll();
            if (node.isVisited()) {
                continue;
            }
            visitedNodes.add(node);
            node.setVisited(true);
            var children = node.getChildren();
            fn.accept(queue, children);
        }

        return visitedNodes;
    }

    public static class RouterNode extends Node<RouterNode, Integer>{

        public RouterNode(Integer data) {
            super(data);
        }
    }

}
