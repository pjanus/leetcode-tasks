package tasks;

import java.util.*;
import java.util.function.Function;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class TopKFrequentlyMentionedKeywords {

    public static void main(String[] args) {
        int k1 = 2;
        String[] keywords1 = { "anacell", "cetracular", "betacellular" };
        String[] reviews1 = { "Anacell provides the best services in the city", "betacellular has awesome services",
                "Best services provided by anacell, everyone should use anacell", };
        int k2 = 2;
        String[] keywords2 = { "anacell", "betacellular", "cetracular", "deltacellular", "eurocell" };
        String[] reviews2 = { "I love anacell Best services; Best services provided by anacell",
                "betacellular has great services", "deltacellular provides much better services than betacellular",
                "cetracular is worse than anacell", "Betacellular is better than deltacellular.", };
        System.out.println(solve(k1, keywords1, reviews1));
        System.out.println(solve(k2, keywords2, reviews2));
    }

    private static List<Key> solve(int n, String[] keywordArr, String[] reviewArr) {
        var reviews = List.of(reviewArr);
        var keywordMap = Stream.of(keywordArr)
                .collect(Collectors.toMap(Function.identity(), (v) -> 0));
        for (String r: reviews) {
            var words = Arrays.stream(r.split("\\W+"))
                    .map(String::toLowerCase)
                    .collect(Collectors.toSet());

            for (String w: words) {
                keywordMap.computeIfPresent(w, (k, v) -> v + 1);
            }
        }

        var set = keywordMap.entrySet().stream()
                .map(e -> new Key(e.getKey(), e.getValue()))
                .collect(Collectors.toCollection(TreeSet::new));

        return set.stream()
                .limit(n)
                .collect(Collectors.toList());
    }

    private static class Key implements Comparable<Key> {

        private final String word;
        private int count;

        public Key (String word, int count) {
            this.word = word;
            this.count = count;
        }

        public Key add() {
            count += 1;
            return this;
        }

        @Override
        public int compareTo(Key o) {

            if (o.count > count) {
                return 1;
            }

            if (o.count < count) {
                return -1;
            }

            return String.CASE_INSENSITIVE_ORDER.compare(this.word, o.word);
        }

        @Override
        public String toString() {
            return word;
        }
    }

}
