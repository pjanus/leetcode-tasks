package tasks;

public class BinaryGap {
    public static void main(String[] args) {
        var s = new Solution();

        System.out.println(s.solution(1041));
        System.out.println(s.solution(348));
        System.out.println(s.solution(999));
        System.out.println(s.solution(1012312));
    }
}

class Solution {
    public int solution(int N) {
        String binary = Integer.toBinaryString(N);
        int i = 0;
        int currentGap = 0;
        int maxGap = 0;

        while (i < binary.length()) {
            char c = binary.charAt(i);

            if (c == '1') {
                maxGap = Math.max(currentGap, maxGap);
                currentGap = 0;
            } else {
                currentGap++;
            }

            i++;
        }

        return maxGap;
    }
}

