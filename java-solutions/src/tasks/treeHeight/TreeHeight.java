package tasks.treeHeight;

import static utils.DebugUtilsKt.exec;

public class TreeHeight {
    public static void main(String[] args) {
        exec(2, () -> new Solution().solution(
                Tree.of(5,
                        Tree.of(3,
                                Tree.of(20, null, null),
                                Tree.of(21, null, null)
                        ),
                        Tree.of(10,
                                Tree.of(1, null, null),
                                null
                        )
                )
        ));
        exec(3, () -> new Solution().solution(
                Tree.of(5,
                        Tree.of(3,
                                Tree.of(20, null, null),
                                Tree.of(21,
                                        Tree.of(2, null, null), null)
                        ),
                        Tree.of(10,
                                Tree.of(1, null, null),
                                null
                        )
                )
        ));
        exec(-1, () -> new Solution().solution(
                null
        ));
        exec(0, () -> new Solution().solution(
                Tree.of(5,null,null)
        ));
        exec(5, () -> new Solution().solution(
                Tree.of(5,
                        Tree.of(3,
                                Tree.of(20, null, null),
                                Tree.of(21,
                                        Tree.of(2, null, null), null)
                        ),
                        Tree.of(10,
                                Tree.of(1, null, null),
                                Tree.of(1,
                                        Tree.of(1,
                                                Tree.of(1,
                                                        Tree.of(1, null, null),
                                                        null
                                                ),
                                                null
                                        ),
                                        null
                                )
                        )
                )
        ));
    }
}
