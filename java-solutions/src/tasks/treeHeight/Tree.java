package tasks.treeHeight;

public class Tree {
    public int x;
    public Tree l;
    public Tree r;

    public static Tree of(int x, Tree l, Tree r) {
        var tree = new Tree();
        tree.l = l;
        tree.r = r;
        return tree;
    }
}