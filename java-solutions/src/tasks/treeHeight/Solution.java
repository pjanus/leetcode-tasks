package tasks.treeHeight;

import static java.lang.Math.max;

public class Solution {
    public int solution(Tree T) {
        return depth(T, -1);
    }

    int depth(Tree t, int d) {
        if (t == null) {
            return d;
        }

        d += 1;

        var ld = depth(t.l, d);
        var rd = depth(t.r, d);

        return max(ld, rd);
    }
}