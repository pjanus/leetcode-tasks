package tasks.commonPrimeDivisors

import utils.exec

fun main() {
    exec(1) { solution(intArrayOf(15, 10, 3), intArrayOf(75, 30, 5)) }
    exec(2) { solution(intArrayOf( 3, 9, 20, 11), intArrayOf(9, 81, 5, 13)) }
    exec(4) { solution(intArrayOf(121, 8, 25, 81, 49), intArrayOf(11, 4, 125, 11, 7)) }
    exec(1) { solution(intArrayOf(22), intArrayOf(88)) }

    // 30, 75
    // 30: 2, 3, 5, 6, 10, 15
    // 30: 2, 3, 5
    // 75: 3, 5, 15, 25
    // 75: 3, 5

    // https://app.codility.com/demo/results/training52GYSQ-Z22/
    // rewrite solution using lowest common divisor
}