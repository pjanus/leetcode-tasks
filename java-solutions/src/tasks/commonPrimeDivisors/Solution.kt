package tasks.commonPrimeDivisors

fun solution(A: IntArray, B: IntArray): Int {
    println("solution A: ${A.joinToString()}, B: ${B.joinToString()}")

    var result = A.size
    for (i in (A.indices)) {
        //println("${A[i]}: ${primeFactors(A[i]).joinToString()}")
        //println("${B[i]}: ${primeFactors(B[i]).joinToString()}")
        val itA = primeFactors(A[i]).iterator()
        val itB = primeFactors(B[i]).iterator()
        while (true) {
            val a = if (itA.hasNext()) itA.next() else null
            val b = if (itB.hasNext()) itB.next() else null

            if (a != b) {
                //println("${A[i]} and ${B[i]} have different divisors")
                result--
                break
            }

            if (a == null) {
                break
            }
        }
    }
    return result;
}

fun primeFactors(a: Int) = sequence {
    var value = a
    var nextPrime = 0
    var known = 0
    while (true) {
        if (value == 1) {
            break
        }
        for (i in primes()) {
            if (value % i == 0) {
                nextPrime = i;
                break
            }
        }

        val divided = value / nextPrime
        if (nextPrime > known) {
            known = nextPrime
            yield(nextPrime)
        }

        if (isPrime(divided)) {
            if (divided > known) {
                known = divided
                yield(divided)
            }
            break
        }

        value = divided
    }
}

fun primes(): Sequence<Int> {
    var i = 0

    return sequence {
        generateSequence { i++ }
                .filter { n -> isPrime(n) }
                .forEach {
                    yield(it)
                }
    }
}

fun isPrime(n: Int) = n > 1 && ((2 until n / 2 + 1).none { i -> n % i == 0 })