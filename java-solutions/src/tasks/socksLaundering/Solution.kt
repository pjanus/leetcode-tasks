// you can also use imports, for example:
import kotlin.math.*

// you can write to stdout for debugging purposes, e.g.
// println("this is a debug message")

fun solution(K: Int, C: IntArray, D: IntArray): Int {
    // this solution is too "clever", I should have done something like this:
    // https://github.com/Nuru/Codility/blob/master/solutions/socks_laundering.rb
    var maxLaunderedSocks = K;
    val cleanSocks = C.groupBy { it }
            .mapValues { it -> it.value.size }.toMutableMap()

    val dirtySocks = D.groupBy { it }
            .mapValues { it -> it.value.size }

    val allSocks = (C + D).groupBy { it }.mapValues { it -> it.value.size }
    val allSocksOrder = allSocks.asSequence()
            .sortedByDescending { val sum = (((cleanSocks[it.key] ?: 0) * 1.01) + min((dirtySocks[it.key]
                    ?: 0), maxLaunderedSocks))

                var decimal = sum - floor(sum)

                (sum - (sum % 2)) + decimal
            }
            .map { Triple(it.key,cleanSocks[it.key] ?: 0, dirtySocks[it.key] ?: 0) }

    println("clean socks: $cleanSocks")
    println("dirty socks: $dirtySocks")
    println("all socks order: ${allSocksOrder.joinToString()}")

    for (it in allSocksOrder) {
        if (maxLaunderedSocks == 0) {
            break;
        }

        val maxCleaned = it.second + min(it.third, maxLaunderedSocks)
        val maxCleanedEven = maxCleaned - maxCleaned % 2
        val toClean = maxCleanedEven - it.second
        maxLaunderedSocks -= toClean
        cleanSocks[it.first] = (cleanSocks[it.first] ?: 0) + toClean
    }

    return cleanSocks.entries.sumBy { it.value / 2 }
}