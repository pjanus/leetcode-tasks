package tasks.socksLaundering

import solution
import utils.exec

fun main() {
    exec(3) { solution(2, intArrayOf(1, 2, 1, 1), intArrayOf(1, 4, 3, 2, 4)) }
    exec(1) { solution(1, intArrayOf(1, 2), intArrayOf(2, 1)) }
    exec(3) { solution(6, intArrayOf(1, 2, 3), intArrayOf(1, 1, 1, 4, 4, 4, 4, 4, 4)) }
    exec(3) { solution(6, intArrayOf(1, 2, 3), intArrayOf(4, 4, 4, 4, 4, 4)) }
    exec(5) { solution(10, intArrayOf(1), intArrayOf(2, 2, 2, 2, 2, 2, 2, 2, 2, 2)) }
    exec(0) { solution(0, intArrayOf(1), intArrayOf(2, 2, 2, 2, 2, 2, 2, 2, 2, 2)) }
    exec(1) { solution(0, intArrayOf(1, 2, 1, 1), intArrayOf(1, 4, 3, 2, 4)) }
    /* this one does not work */ exec(33) {solution(25,
            intArrayOf(44, 28, 15, 40, 46, 27, 46, 6, 41, 28, 40, 29, 11, 38, 2, 48, 23, 32, 1, 45, 21, 34, 24, 27, 23, 26, 30, 46, 3, 20, 18, 50, 3, 40, 26, 25, 1, 22, 9, 31, 49, 34, 13, 9, 23, 8, 46, 40, 19, 17),
            intArrayOf(3, 48, 29, 23, 14, 7, 8, 11, 26, 6, 11, 41, 49, 32, 20, 45, 14, 46, 11, 9, 5, 13, 34, 3, 46, 15, 30, 45, 20, 46, 32, 2, 25, 46, 11, 3, 13, 13, 49, 1, 18, 22, 36, 33, 2, 22, 42, 45, 39, 32)
    )}


}