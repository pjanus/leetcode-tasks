package tasks;

import utils.Position;

import java.util.LinkedList;

public class MaxAreaOfIsland {

    public static void main(String[] args) {
        int[][] grid = new int[][] {
             new int[] {0,0,1,0,0,0,0,1,0,0,0,0,0},
             new int[] {0,0,0,0,0,0,0,1,1,1,0,0,0},
             new int[] {0,1,1,0,1,0,0,0,0,0,0,0,0},
             new int[] {0,1,0,0,1,1,0,0,1,0,1,0,0},
             new int[] {0,1,0,0,1,1,0,0,1,1,1,0,0},
             new int[] {0,0,0,0,0,0,0,0,0,0,1,0,0},
             new int[] {0,0,0,0,0,0,0,1,1,1,0,0,0},
             new int[] {0,0,0,0,0,0,0,1,1,0,0,0,0}
        };

        test(maxAreaOfIsland(grid), 6);

        int[][] grid2 = new int[][] {
            new int[] {0,0,0,0,0,0,0,0}
        };

        test(maxAreaOfIsland(grid2), 0);

    }

    public static void test(int actual, int expected) {
        String result = "";
        result = actual == expected
                ? actual + " Git! :)"
                : actual + " Lipa! :( Powinno być " + expected;

        System.out.println(result);
    }

    public static int maxAreaOfIsland(int[][] gridArr) {
        int count = 0;
        var biggestIsland = 0;
        final int limitY = gridArr.length;
        final int limitX = limitY > 0 ? gridArr[0].length : 0;

        if (limitX == 0) {
            return 0;
        }

        final var queue = new LinkedList<Position>();

        for (int y = 0; y < limitY; y++) {
            for (int x = 0; x < limitX; x++) {
                var node = gridArr[y][x];

                if (node == 1) {
                    count++;
                    queue.offer(new Position(x, y));
                    int size = 0;
                    while (!queue.isEmpty()) {
                        final var pos = queue.poll();
                        if (gridArr[pos.y][pos.x] == 2) {
                            continue;
                        }
                        size++;
                        gridArr[pos.y][pos.x] = 2;
                        final var top = pos.withYDiff(-1);
                        final var left = pos.withXDiff(-1);
                        final var bottom = pos.withYDiff(1);
                        final var right = pos.withXDiff(1);

                        if(isPartOfCurrentIsland(top, limitX, limitY, gridArr)) {
                            queue.offer(top);
                        }

                        if(isPartOfCurrentIsland(left, limitX, limitY, gridArr)) {
                            queue.offer(left);
                        }

                        if(isPartOfCurrentIsland(bottom, limitX, limitY, gridArr)) {
                            queue.offer(bottom);
                        }

                        if(isPartOfCurrentIsland(right, limitX, limitY, gridArr)) {
                            queue.offer(right);
                        }
                        if (size > biggestIsland) {
                            biggestIsland = size;
                        }
                    }

                }
            }
        }
        return biggestIsland;
    }

    private static boolean isPartOfCurrentIsland(Position pos, int limitX, int limitY, int[][] gridArr) {
        return pos.x >= 0 && pos.x < limitX &&
                pos.y >= 0 && pos.y < limitY &&
                gridArr[pos.y][pos.x] == 1;
    }
}
