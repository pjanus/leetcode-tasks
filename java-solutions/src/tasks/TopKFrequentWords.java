package tasks;

import java.util.*;
import java.util.function.Function;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class TopKFrequentWords {

    public static void main(String[] args) {

    }

    public List<String> topKFrequent(String[] words, int k) {
        Comparator<Map.Entry<String, Integer>> comparator = (e1, e2) -> {
            if (e1.getValue().equals(e2.getValue())) {
                return String.CASE_INSENSITIVE_ORDER.compare(e1.getKey(), e2.getKey());
            }

            return e1.getValue() - e2.getValue();
        };

        var map = Arrays.asList(words).stream()
                .collect(Collectors.toMap(Function.identity(), i -> 1, Integer::sum));

        return map.entrySet()
                .stream()
                .sorted(comparator)
                .map(Map.Entry::getKey)
                .limit(k)
                .collect(Collectors.toList());
    }
}
