package tasks.RectangleBuilderGreaterArea;

import java.util.*;

public class Solution {
    public int solution(int[] A, int X) {
        var map = new HashMap<Integer, Integer>();
        var sortedMap = new TreeMap<Integer, Integer>(Comparator.reverseOrder());
        var count = 0;

        for (int i : A) {
            var newValue = map.compute(i, (k, v) -> {
                if (v == null) {
                    return 1;
                }

                return v+1;
            });

            if (newValue >= 2) {
                sortedMap.compute(i, (k, v) -> Math.min(2, newValue / 2));
            }
        }

        debug("original value/count %s", map);

        if (sortedMap.isEmpty()) {
            return count;
        }

        while (!sortedMap.isEmpty()) {
            debug("sorted value/pair count %s",  sortedMap);
            var first = sortedMap.firstEntry();

            if (Math.pow(first.getKey(), 2) < X) {
                debug("leaving cause %d^2 < %d", first.getKey(), X);
                break;
            }

            switch (first.getValue()) {
                case 2:
                    debug("adding 2x self pair");
                    count++;
                case 1:
                    var min = possibleMin(X, sortedMap);
                    sortedMap.navigableKeySet()
                            .removeIf(k -> k < min);

                    debug("adding subMap %s of size %d", sortedMap, sortedMap.size());

                    count += sortedMap.size() - 1; // -1 cause excluding HEAD
            }

            sortedMap.pollFirstEntry();
        }

        return count;
    }

    private int possibleMin(int X, NavigableMap<Integer, Integer> sortedMap) {
        return (int) Math.ceil(X / (double) sortedMap.firstKey());
    }
    
    private void debug(String s, Object... input) {
        //System.out.println(String.format(String.format("--- %s", s), input));
    }
}
