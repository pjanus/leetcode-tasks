package tasks.RectangleBuilderGreaterArea;

import java.util.Arrays;

public class RectangleBuilderGreaterArea {

    public static void main(String[] args) {
        solution(new int[]{1, 2, 5, 1, 1, 2, 3, 5, 1}, 5, 2);
        solution(new int[]{1, 2, 1, 1, 2, 3, 1, 1, 1, 1, 3}, 5, 1);
        solution(new int[]{1, 2, 3, 4, 5, 6, 7}, 5, 0);
        solution(new int[]{1, 1, 1, 1, 1, 1}, 5, 0);
        solution(new int[]{5, 5, 5, 5, 5, 5, 5, 5, 5}, 5, 1);
        solution(new int[]{10, 10, 10, 10, 7, 7, 7, 7}, 5, 3);
        solution(new int[]{10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 7, 7, 7, 7, 7, 7, 7, 7, 7}, 5, 3);
        solution(new int[]{10, 10, 10, 10, 7, 7, 7, 7, 5, 5, 5, 5}, 5, 6);
    }

    private static void solution(int[] A, int X, int expected) {
        System.out.println(String.format("solution for: %s, %d", Arrays.toString(A), X));
        System.out.println(String.format("expected: %d result: %d", expected, new Solution().solution(A, X)));
    }
}
