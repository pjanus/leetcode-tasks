package tasks.dwarfRafting

fun main() {

    exec(N = 4, S = "1B 1C 4B 1D 2A", T = "3B 2D", expected = 6)
    exec(N = 4, S = "1A 1B 2A 2B", T = "3B 2D", expected = 6)
    exec(N = 2, S = "", T = "", expected = 4)
    exec(N = 4, S = "", T = "", expected = 16)
    exec(N = 6, S = "", T = "", expected = 36)
    exec(N = 8, S = "", T = "", expected = 64)

    exec(arrayOf(
            arrayOf('D', 'D', 'D', 'D'),
            arrayOf('D', 'D', 'D', 'D'),
            arrayOf('D', 'D', 'D', 'D'),
            arrayOf('D', 'D', 'D', 'D')
    ), 0)

    exec(arrayOf(
            arrayOf('B', 'B', 'B', 'B'),
            arrayOf('B', 'B', 'B', 'B'),
            arrayOf('B', 'B', 'B', 'B'),
            arrayOf('B', 'B', 'B', 'B')
    ), 0)

    exec(arrayOf(
            arrayOf('0', 'D', 'D', 'D'),
            arrayOf('D', '0', 'D', 'D'),
            arrayOf('D', 'D', 'D', 'D'),
            arrayOf('D', 'D', 'D', 'D')
    ), 2)

    exec(arrayOf(
            arrayOf('B', 'B', 'D', 'D'),
            arrayOf('B', 'B', 'D', 'D'),
            arrayOf('D', 'D', 'D', 'D'),
            arrayOf('D', 'D', 'D', 'D')
    ), -1)

    exec(arrayOf(
            arrayOf('B', 'D', 'B', 'D'),
            arrayOf('D', 'B', 'B', 'B'),
            arrayOf('B', 'B', 'B', 'B'),
            arrayOf('B', 'D', 'D', 'D')
    ), 0)

    exec(arrayOf(
            arrayOf('0', 'D', '0', 'D'),
            arrayOf('D', '0', 'B', '0'),
            arrayOf('0', '0', 'B', 'B'),
            arrayOf('0', 'D', 'D', 'D')
    ), 4)
}

fun exec(grid: Array<Array<Char>>, expected: Int) {
    val (N, S, T) = grid2Input(grid)
    println(grid.joinToString(separator = "\n", transform = { it.joinToString(separator = " ")}))
    println()
    exec(N, S, T, expected)
}

fun exec(N: Int, S: String, T: String, expected: Int) {
    val result = solution(N, S, T)

    if (result == expected) {
        println("success! Got: $result")
    } else {
        println("failure! Expected $expected and got: $result !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!")
    }
    println("--------------------------------------------------------------------------")
}

data class Output(val N: Int, val S: String, val T: String)

fun grid2Input(grid: Array<Array<Char>>): Output {
    var barrels = ""
    var dwarfs = ""
    var size = grid.size

    for ((rowIdx, row) in grid.withIndex()) {
        for ((colIdx, cell) in row.withIndex()) {
            if (cell == 'B') {
                barrels += buildSymbol(rowIdx, colIdx)
            }

            if (cell == 'D') {
                dwarfs += buildSymbol(rowIdx, colIdx)
            }
        }
    }
    return Output(size, barrels, dwarfs)
}

private fun buildSymbol(rowIdx: Int, colIdx: Int) = " ${rowIdx + 1}${('A' + colIdx)}"