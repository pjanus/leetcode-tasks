package tasks.dwarfRafting

import kotlin.math.min
import kotlin.math.pow

fun solution(N: Int, S: String, T: String): Int {
    println("Arguments N=${N} S=\"${S}\" T=\"${T}\"")
    val quarterDim = N / 2.0
    val quarterSize = quarterDim.pow(2)
    val barrels = S.split(" ")
    val dwarfs = T.split(" ")
    val quarterSpots = arrayOf(quarterSize, quarterSize, quarterSize, quarterSize)

    println("quarterSize = $quarterSize")
    println("quarterDim = $quarterDim")

    for (barrel in barrels) {
        if (barrel.isEmpty()) {
            continue
        }
        val (x, y) = splitPosition(barrel)
        val quarterIdx = selectQuarter(x, y, quarterDim)

        println("adding barrel $barrel ($x, $y) to quarter $quarterIdx")

        quarterSpots[quarterIdx]--
    }

    val quarterMaxDwarfs = arrayOf(
            min(quarterSpots[0], quarterSpots[3]).toInt(),
            min(quarterSpots[1], quarterSpots[2]).toInt(),
            min(quarterSpots[1], quarterSpots[2]).toInt(),
            min(quarterSpots[0], quarterSpots[3]).toInt()
    )

    println("quarterMaxDwarfs:")
    println("${quarterMaxDwarfs[0].toString()} ${quarterMaxDwarfs[1].toString()}")
    println("${quarterMaxDwarfs[2].toString()} ${quarterMaxDwarfs[3].toString()}")

    for (dwarf in dwarfs) {
        if (dwarf.isEmpty()) {
            continue
        }
        val (x, y) = splitPosition(dwarf)
        val quarterIdx = selectQuarter(x, y, quarterDim)

        println("adding dwarf $dwarf ($x, $y) to quarter $quarterIdx")
        quarterMaxDwarfs[quarterIdx]--;

        if (quarterMaxDwarfs[quarterIdx] < 0) {
            println("quarter $quarterIdx is unbalanced")
            println("position $dwarf ($x, $y)")
            return -1
        }
    }

    return quarterMaxDwarfs.sum()
}

fun splitPosition (symbol: String): Pair<Int, Int> {
    val (y, x) = "(\\d{1,2})([A-Z])".toRegex().find(symbol.trim())!!.destructured
    return Pair(letterToIndex(x), to0index(y))
}
fun to0index (s: String) = s.toInt() - 1
fun letterToIndex (s: String): Int = s[0] - 'A'

fun selectQuarter(x: Int, y: Int, quarterDim: Double): Int {
    return when {
        x < quarterDim && y < quarterDim -> 0
        x >= quarterDim && y < quarterDim -> 1
        x < quarterDim && y >= quarterDim -> 2
        x >= quarterDim && y >= quarterDim -> 3
        else -> throw RuntimeException("($x, $y) does not fit in any quarter?!?")
    }
}

