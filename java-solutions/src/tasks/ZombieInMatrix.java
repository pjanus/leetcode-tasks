package tasks;

import utils.Grid;
import utils.Position;

import java.util.*;
import java.util.stream.Collectors;

import static java.util.function.Predicate.not;

public class ZombieInMatrix {

    public static void main(String[] args) {
        System.out.println(minHours(4, 5,
                List.of(List.of(0, 1, 1, 0, 1),
                        List.of(0, 1, 0, 1, 0),
                        List.of(0, 0, 0, 0, 1),
                        List.of(0, 1, 0, 0, 0))
                )
        );
    }

    public static int minHours(int rows, int columns, List<List<Integer>> gridArr) {
        var grid = generateGrid(gridArr);
        var currentGen = getFirstGenerationOfZombies(grid);
        var hours = 0;
        var nextGen = new LinkedList<Being>();

        do {
            while (!currentGen.isEmpty()) {
                var being = currentGen.poll();
                var humans = being.getSurroundingHumans(grid);
                humans.forEach(Being::turnZombie);
                nextGen.addAll(humans);
            }
            currentGen = new LinkedList<>(nextGen);
            nextGen.clear();
            if (!currentGen.isEmpty()) {
                hours++;
            }
        } while (!currentGen.isEmpty());

        return hours;
    }

    private static Grid<Being> generateGrid(List<List<Integer>> grid) {
        var map = new Grid<Being>();
        for (int i = 0; i < grid.size(); i++) {
            List<Integer> row = grid.get(i);
            for (int i1 = 0; i1 < row.size(); i1++) {
                Integer node = row.get(i1);
                var position = new Position(i1, i);
                map.put(position, Being.create(position, node));
            }
        }

        return map;
    }

    private static Queue<Being> getFirstGenerationOfZombies(Grid<Being> grid) {
        return grid.values().stream()
                .filter(v -> v.zombie)
                .collect(Collectors.toCollection(LinkedList::new));
    }

    private static class Being {
        private final Position position;
        private boolean zombie;

        private Being(Position position, boolean zombie) {
            this.position = position;
            this.zombie = zombie;
        }

        public static Being create(Position position, int isZombie) {
            return new Being(position, isZombie == 1);
        }

        public boolean isZombie() {
            return zombie;
        }

        public void turnZombie() {
            this.zombie = true;
        }

        public List<Being> getSurroundingHumans(Grid<Being> grid) {
            var x = position.x;
            var y = position.y;
            var children = List.of(
                    new Position(x, y - 1), // top
                    new Position(x + 1, y), // right
                    new Position(x, y + 1), // bottom
                    new Position(x - 1, y) // left
            );

            return children.stream()
                    .map(grid::get)
                    .filter(Objects::nonNull)
                    .filter(not(Being::isZombie))
                    .collect(Collectors.toList());
        }
    }

}
